module Sovren
  class Client
    attr_reader :endpoint, :account_id, :service_key, :configuration, :revision_date, :skill_data

    #Initialize the client class that will be used for all sovren requests.
    #
    # @param [Hash] options
    # @option options String :endpoint The url that the web service is located at
    # @option options String :account_id The account id for the webservice
    # @option options String :service_key The service key for the webservice
    # @option options Integer :configuration The parser configuration params, used to tweak the output of the parser
    # @option options String :revision_date The revision date of the resume in YYYY-MM-DD format. This is useful when parsing older resumes.
    def initialize(options={})
      @endpoint = options[:endpoint]
      @account_id = options[:account_id]
      @service_key = options[:service_key]
      @configuration = options[:configuration] || "_000000_0_00000001_1101010110001100_1_0000000000000111111102000000000010000100000000000000000000100"
      @revision_date = options[:revision_date]
    end

    def connection
      Savon.client(wsdl: endpoint, log: false) do
        convert_request_keys_to :none
      end
    end

    def parse(file, include_raw_text = false)
      result = connection.call(:parse_resume) do |c|
        c.message(
          request: {
            AccountId: account_id,
            ServiceKey: service_key,
            FileBytes: Base64.encode64(file),
            Configuration: configuration,
            RevisionDate: revision_date
          }
        )
      end

      parse_resume_result = result.body[:parse_resume_response][:parse_resume_result]
      unless parse_resume_result[:code] == 'Success'
        raise StandardError, "Error parsing Resume. Code: #{parse_resume_result[:code]}. Message: #{parse_resume_result[:Message]}"
      end

      if include_raw_text
        [Resume.parse(parse_resume_result[:xml]), parse_resume_result[:text]]
      else
        Resume.parse(parse_resume_result[:xml])
      end
    end

    def convert(file, format)
      result = connection.call(:do_conversion_simplified) do |c|
        c.message({
          "DocumentAsByteArray" => Base64.encode64(file),
          "OutputType" => format})
      end

      result.body[:do_conversion_simplified_response][:do_conversion_simplified_result].to_s
    end
  end
end